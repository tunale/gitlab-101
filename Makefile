TARGET=hello

all: ${TARGET}

hello: ${TARGET}.c
	echo "#define TODAY \"`date`\"" | tee hello.h
	${CC} ${TARGET}.c -static -o ${TARGET}

clean:
	rm ${TARGET}